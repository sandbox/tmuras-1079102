/**
 * @file
 * README file for TACL.
 */

TACL
Taxonomy based ACL.

----
tacl module is API-only module. It provides functions for managing taxonomy-based access to the nodes.

----
tacl_user is a front-end module for tacl that is directly usable by site builders/administrators.
It will allow configuring a subset of tacl possible settings - it will allow to set permissions
on per-user basis.
