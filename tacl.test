<?php

/**
 * @file
 */

/**
 * Base class with some helper functions.
 */
class TaclTest extends DrupalWebTestCase {
  function createNode(&$edit, $type='page') {
    if (!isset($edit['title'])) {
      $edit['title'] = $this->randomName(8);
    }
    if (!isset($edit["body[und][0][value]"])) {
      $edit["body[und][0][value]"] = $this->randomName(16);
    }
    $this->drupalPost("node/add/$type", $edit, t('Save'));
    $this->assertText(t('@title has been created.', array('@title' => $edit['title'])), "User created new '$type' node.");

    $matches = NULL;
    preg_match('|node/(\d+)|', $this->url, $matches);
    $nid = (int) $matches[1];
    if (!$nid) {
      $this->fail('Couldn\'t find an id of newly created node.');
    }
    return $nid;
  }

  function createUser($roles) {
    $edit = array();
    $edit['name'] = $this->randomName();
    $edit['mail'] = $edit['name'] . '@example.com';
    $edit['roles'] = $roles;
    $edit['pass'] = user_password();
    $edit['status'] = 1;

    $account = user_save(drupal_anonymous_user(), $edit);

    $this->assertTrue(!empty($account->uid), t('User created with name %name and pass %pass', array('%name' => $edit['name'], '%pass' => $edit['pass'])), t('User login'));
    if (empty($account->uid)) {
      return FALSE;
    }

    // Add the raw password so that we can log in as this user.
    $account->pass_raw = $edit['pass'];
    return $account;
  }

  function createVocabulary() {
    // Create a vocabulary.
    $vocabulary = new stdClass();
    $vocabulary->name = $this->randomName();
    $vocabulary->description = $this->randomName();
    $vocabulary->machine_name = drupal_strtolower($this->randomName());
    $vocabulary->help = '';
    $vocabulary->nodes = array('article' => 'article');
    $vocabulary->weight = mt_rand(0, 10);
    taxonomy_vocabulary_save($vocabulary);

    return $vocabulary;
  }

  /**
   * Returns a new term with random properties in vocabulary $vid.
   */
  function createTerms($vocabulary) {
    $terms = array();
    for ($i = 0; $i < 10; $i++) {
      $term = new stdClass();
      $term->name = $this->randomName();
      $term->description = $this->randomName();
      // Use the first available text format.
      $term->format = db_query_range('SELECT format FROM {filter_format}', 0, 1)->fetchField();
      $term->vid = $vocabulary->vid;
      taxonomy_term_save($term);
      $terms[] = $term;
    }

    return $terms;
  }

  /**
   * Add taxonomy field ($vocabulary) to content type
   * @param string $contentType
   */
  function addTaxonomyField($vocab, $contentType) {
    $field_name = 'field_tax_' . $vocab->machine_name;

    $taxonomy_field = array(
      'field_name' => $field_name,
      'type' => 'taxonomy_term_reference',
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => $vocab->machine_name,
            'parent' => '0',
          ),
        ),
      ),
    );
    field_create_field($taxonomy_field);

    $taxonomy_instance = array(
      'entity_type' => 'node',
      'bundle' => $contentType,
      'field_name' => $field_name,
      'type' => 'taxonomy_term_reference',
      'widget' => array(
        'type' => 'options_select',
      ),
    );
    field_create_instance($taxonomy_instance);

    node_types_rebuild();

    return $field_name;
  }

}

/**
 * Testing combinations of default settings per vocabulary and role.
 */
class TaclDefaultsTestCase extends TaclTest {

  protected $user_admin, $user_role1, $user_role2, $user_role12;
  protected $role1, $role2;
  protected $content_type1, $content_type2;
  protected $vocab1, $vocab2;
  protected $terms1, $terms2;
  protected $tax_field1, $tax_field2;

  public static function getInfo() {
    return array(
      'name' => 'Test defaults',
      'description' => 'Testing default settings for TACL vocabularies.',
      'group' => 'TACL',
    );
  }

  public function setUp() {
    parent::setUp('tacl');  // Enable any modules required for the test

    $this->role1 = $this->drupalCreateRole(array('create page content', 'delete any page content', 'delete own page content', 'edit any page content', 'edit own page content'));
    $this->role2 = $this->drupalCreateRole(array('create page content', 'delete any page content', 'delete own page content', 'edit any page content', 'edit own page content'));

    $this->user_admin = $this->drupalCreateUser(array('bypass node access'));
    $this->user_role1 = $this->createUser(array($this->role1 => $this->role1));
    $this->user_role2 = $this->createUser(array($this->role2 => $this->role2));
    $this->user_role12 = $this->createUser(array($this->role1 => $this->role1, $this->role2 => $this->role2));

    $this->vocab1 = $this->createVocabulary();
    $this->terms1 = $this->createTerms($this->vocab1);

    $this->vocab2 = $this->createVocabulary();
    $this->terms2 = $this->createTerms($this->vocab2);

    $types = node_type_get_types();
    $this->content_type1 = $types['page'];
    $this->content_type2 = $types['article'];

    $this->tax_field1 = $this->addTaxonomyField($this->vocab1, 'page');
    $this->tax_field2 = $this->addTaxonomyField($this->vocab2, 'article');
  }

  /**
   * Test that user is not allowed to change taxonomy terms,
   * when this is denied by the TACL setting.
   */
  public function testTaxonomyChangeNotAllowed() {
    $defaults = Tacl::$defaults;
    $defaults['grant_create'] = 0;
    tacl_set_vid_defaults($this->vocab1->vid, $defaults);
    $this->drupalLogin($this->user_role1);

    //allowed to add with no taxonomy set
    $edit['title'] = $this->randomName(8);
    $edit["body[und][0][value]"] = $this->randomName(16);
    unset($edit[$this->tax_field1 . "[und]"]);
    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(t('@title has been created.', array('@title' => $edit['title'])), 'User allowed to create new node with no taxonomy set');

    //not allowed to change/update taxonomy while editing
    $this->clickLink(t('Edit'));
    $edit[$this->tax_field1 . "[und]"] = $this->terms1[0]->tid;
    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText('You are not allowed to edit some terms', 'User not allowed to edit taxonomy for existing node.');

    //not allowed with any value
    $edit[$this->tax_field1 . "[und]"] = $this->terms1[0]->tid;
    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText('You are not allowed to edit some terms', 'User not allowed to create new node with taxonomy set.');

    tacl_clear_vocab($this->vocab1->vid);
  }

  /**
   * Test that user is allowed to change taxonomy terms.
   */
  public function testTaxonomyChangeAllowed() {
    $defaults = Tacl::$defaults;
    tacl_set_vid_defaults($this->vocab1->vid, $defaults);
    $this->drupalLogin($this->user_role1);

    //allowed to add without taxonomy term set
    $edit['title'] = $this->randomName(8);
    $edit["body[und][0][value]"] = $this->randomName(16);
    unset($edit[$this->tax_field1 . "[und]"]);
    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(t('@title has been created.', array('@title' => $edit['title'])), 'User allowed to create new node with no taxonomy set');

    //allowed to add with taxonomy set
    $edit['title'] = $this->randomName(8);
    $edit["body[und][0][value]"] = $this->randomName(16);
    $edit[$this->tax_field1 . "[und]"] = $this->terms1[0]->tid;
    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(t('@title has been created.', array('@title' => $edit['title'])), 'User allowed to create new node with no taxonomy set');

    //allowed to change/update taxonomy while editing
    $this->clickLink(t('Edit'));
    $edit[$this->tax_field1 . "[und]"] = $this->terms1[1]->tid;
    $this->drupalPost(NULL, $edit, t('Save'));
    $this->assertText(t('@title has been updated', array('@title' => $edit['title'])), 'User allowed to edit taxonomy for existing node.');

    tacl_clear_vocab($this->vocab1->vid);
  }

  /**
   * Test that user is not allowed to view the node, when denied by TACL defaults.
   * @TODO implement
   */
  public function testViewNotAllowed() {
    $defaults = Tacl::$defaults;
    tacl_set_vid_defaults($this->vocab1->vid, $defaults);
    $this->drupalLogin($this->user_role1);

    tacl_clear_vocab($this->vocab1->vid);
  }

  /**
   * Test that user is not allowed to update a node with taxonomy for which TACL denies it by default.
   */
  public function testUpdateNotAllowed() {
    $defaults = Tacl::$defaults;
    $defaults['grant_update'] = 2;
    tacl_set_vid_defaults($this->vocab1->vid, $defaults);
    $this->drupalLogin($this->user_role1);

    //add a new node
    $edit['title'] = $this->randomName(8);
    $edit["body[und][0][value]"] = $this->randomName(16);
    $edit[$this->tax_field1 . "[und]"] = $this->terms1[0]->tid;
    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(t('@title has been created.', array('@title' => $edit['title'])), 'User allowed to create new node with no taxonomy set');

    //not allowed to edit the node
    $this->assertNoLink(t('Edit'));

    tacl_clear_vocab($this->vocab1->vid);
  }

  /**
   * Test that user is not allowed to delete a node with taxonomy for which TACL denies it by default.
   */
  public function testDeleteNotAllowed() {
    $defaults = Tacl::$defaults;
    $defaults['grant_delete'] = 2;
    tacl_set_vid_defaults($this->vocab1->vid, $defaults);
    $this->drupalLogin($this->user_role1);

    //add a new node
    $edit[$this->tax_field1 . "[und]"] = $this->terms1[0]->tid;
    $nid = $this->createNode($edit);

    //test deletion
    $this->drupalGet("node/$nid/delete");
    $this->assertText(t('Access denied'));
    //$this->assertText($edit['title'] . ' has been deleted');

    tacl_clear_vocab($this->vocab1->vid);
  }

  /**
   * User is allowed to manage one term.
   */
  public function testRoleCombinations() {
    //all denies by default
    $defaults = array(
      'grant_view' => 2,
      'grant_update' => 2,
      'grant_delete' => 2,
      'grant_create' => 0,
      'grant_list' => 0,
    );
    tacl_set_vid_defaults($this->vocab1->vid, $defaults);

    //role 1 allowed to view
    $defaults = array(
      'grant_view' => 1,
      'grant_update' => 2,
      'grant_delete' => 2,
      'grant_create' => 0,
      'grant_list' => 0,
    );
    tacl_set_vid_defaults($this->vocab1->vid, $defaults, $this->role1);

    $this->drupalLogin($this->user_admin);
    $edit[$this->tax_field1 . "[und]"] = $this->terms1[0]->tid;
    $nid = $this->createNode($edit);

    //user 1 can see it
    $this->drupalLogin($this->user_role1);
    $this->drupalGet("node/$nid");
    $this->assertText($edit['title'], 'User with role 1 can see the node.');

    //user 12 as well
    $this->drupalLogin($this->user_role12);
    $this->drupalGet("node/$nid");
    $this->assertText($edit['title'], 'User with role 1 and 2 can see the node.');

    //but user 2 can not
    $this->drupalLogin($this->user_role2);
    $this->drupalGet("node/$nid");
    $this->assertText(t('Access denied'), 'User with role 2 can not see the node.');

    tacl_clear_vocab($this->vocab1->vid);
  }

}

class TaclBareTestCase extends TaclTest {

  protected $user_role1, $user_role2, $user_role12;
  protected $role1, $role2;
  protected $content_type1, $content_type2;
  protected $vocab1, $vocab2;
  protected $terms1, $terms2;

  public static function getInfo() {
    return array(
      'name' => 'No TACL settings',
      'description' => 'TACL module enabled but no settings.',
      'group' => 'TACL',
    );
  }

  public function setUp() {
    parent::setUp('tacl');  // Enable any modules required for the test

    $this->role1 = $this->drupalCreateRole(array('create page content', 'delete any page content', 'delete own page content', 'edit any page content', 'edit own page content'));
    $this->role2 = $this->drupalCreateRole(array('create page content', 'delete any page content', 'delete own page content', 'edit any page content', 'edit own page content'));

    $this->user_role1 = $this->createUser(array($this->role1 => $this->role1));
    //$this->user_role1 = user_load($this->user_role1->uid, TRUE);

    $this->user_role2 = $this->createUser(array($this->role2 => $this->role2));
    //$this->user_role2 = user_load($this->user_role2->uid, TRUE);

    $this->user_role12 = $this->createUser(array($this->role1 => $this->role1, $this->role2 => $this->role2));
    //$this->user_role12 = user_load($this->user_role12->uid, TRUE);
    //create 2 vocabularies with terms
    $this->vocab1 = $this->createVocabulary();
    $this->terms1 = $this->createTerms($this->vocab1);

    $this->vocab2 = $this->createVocabulary();
    $this->terms2 = $this->createTerms($this->vocab2);

    $this->tax_field1 = $this->addTaxonomyField($this->vocab1, 'page');
    $this->tax_field2 = $this->addTaxonomyField($this->vocab2, 'article');
  }

  /**
   * Make sure that users can create content as normal.
   */
  public function testContentCreate() {

    $edit = array();
    $edit['title'] = $this->randomName(8);
    $edit["body[und][0][value]"] = $this->randomName(16);
    $edit[$this->tax_field1 . "[und]"] = $this->terms1[0]->tid;

    $this->drupalLogin($this->user_role1);
    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(t('@title has been created.', array('@title' => $edit['title'])));

    $this->drupalLogin($this->user_role2);
    $edit[$this->tax_field1 . "[und]"] = $this->terms1[1]->tid;
    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(t('@title has been created.', array('@title' => $edit['title'])));

    $this->drupalLogin($this->user_role12);
    $edit[$this->tax_field1 . "[und]"] = $this->terms1[2]->tid;
    $this->drupalPost('node/add/page', $edit, t('Save'));
    $this->assertText(t('@title has been created.', array('@title' => $edit['title'])));
  }

}

